\documentclass[11pt, book]{memoir}

\settrims{0pt}{0pt} % page and stock same size
\settypeblocksize{*}{34.5pc}{*} % {height}{width}{ratio}
\setlrmargins{*}{*}{1} % {spine}{edge}{ratio}
\setulmarginsandblock{1in}{1in}{*} % height of typeblock computed
\setheadfoot{\onelineskip}{2\onelineskip} % {headheight}{footskip}
\setheaderspaces{*}{1.5\onelineskip}{*} % {headdrop}{headsep}{ratio}
\checkandfixthelayout

\chapterstyle{bianchi}
\newcommand{\titlefont}{\normalfont\Huge\bfseries}
\renewcommand{\chaptitlefont}{\titlefont}

%-------- Packages --------%

\usepackage{mathtools}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{accents}
\usepackage{newpxtext}
\usepackage[varg,bigdelims]{newpxmath}
\usepackage{eucal}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage{tikz}
\usepackage[siunitx]{circuitikz}
\usepackage{graphicx}
\usepackage{outline}
\usepackage{varwidth}
\usepackage[inline]{enumitem}
\usepackage{ifthen}
\usepackage{footnote}
\usepackage[utf8]{inputenc} %allows non-ascii in bib file
\usepackage[bookmarks=true, colorlinks=true, linkcolor=blue!50!black,
citecolor=orange!50!black, urlcolor=orange!50!black, pdfencoding=unicode]{hyperref}
\usepackage{subfiles}
\usepackage[capitalize]{cleveref}
\usepackage[backend=biber, backref=true, maxbibnames = 10, style = alphabetic]{biblatex}
\usepackage{makeidx}
\usepackage[all]{xy}
\usepackage[framemethod=tikz]{mdframed}
\usepackage{todonotes}
\usepackage{tablefootnote}
%\usepackage{changepage} %indented paragraphs in solutions
%\usepackage{showkeys} %for drafting; prints labels in margin

%-------- Package setup --------%

% cleveref %
  \newcommand{\creflastconjunction}{, and\nobreakspace} % serial comma

% biblatex %
  \addbibresource{Library20180913.bib}

% makeidx %
  \makeindex

% hyperref %
  \hypersetup{final}

% enumitem %
  \setlist{nosep}

% footnote
  \makesavenoteenv{tabular}


% tikz %
  \usetikzlibrary{
    cd,
    math,
    decorations.markings,
    decorations.pathreplacing,
    positioning,
    arrows.meta,
    shapes,
    shadows,
    shadings,
    calc,
    fit,
    quotes,
    intersections,
    circuits,
    circuits.ee.IEC
  }

  \tikzcdset{arrow style=tikz, diagrams={>=To}}

% mdframed/tablefootnote%
% This makes \tablefootnote allow construction of footnotes that appear at bottom of page instead of inside frame

\makeatletter
\AfterEndEnvironment{mdframed}{%
  \tfn@tablefootnoteprintout%
  \gdef\tfn@fnt{0}%
}
\makeatother

% TikZ Stuff

\input{tikz_stuff}

% Theorem environments

% Colored box background colours

\colorlet{theoremcolor}{white!92!blue}
\colorlet{definitioncolor}{white!92!purple}
\colorlet{examplecolor}{white!93!green}

\mdfdefinestyle{theoremframe}{
    linewidth=0pt,
    %linecolor=blue,
    backgroundcolor=theoremcolor,
    roundcorner=6pt,
    nobreak=true,
    leftmargin=0,
    innerleftmargin=0,
    rightmargin=0,
    innerrightmargin=0,
    }

\mdfdefinestyle{definitionframe}{
    linewidth=0pt,
    %linecolor=blue,
    backgroundcolor=definitioncolor,
    roundcorner=6pt,
    leftmargin=0,
    innerleftmargin=0,
    rightmargin=0,
    innerrightmargin=0,
    }

\mdfdefinestyle{exampleframe}{
    linewidth=0pt,
    backgroundcolor=examplecolor,
    leftmargin=0,
    innerleftmargin=0,
    rightmargin=0,
    innerrightmargin=0,
    }

%\mdfdefinestyle{exerciseframe}{
%    linecolor=white!93!yellow,
%    backgroundcolor=white!93!yellow,
%    }

\newtheoremstyle{plain}
  {-\topsep}       % space above
  {}               % space below
  {\normalfont}    % body font
  {}               % indent amount
  {\bfseries}      % theorem head font
  {.}              % punctuation after theorem head
  {.5em}           % space after theorem head
  {}               % theorem head spec

% amsthm %
  \theoremstyle{plain}
  \newmdtheoremenv[style=theoremframe]{theorem}[equation]{Theorem}
  \newmdtheoremenv[style=theoremframe]{proposition}[equation]{Proposition}
  \newmdtheoremenv[style=theoremframe]{corollary}[equation]{Corollary}
  \newmdtheoremenv[style=theoremframe]{lemma}[equation]{Lemma}

  \theoremstyle{plain}
  \newmdtheoremenv[style=definitionframe]{definition}[equation]{Definition}
  \newmdtheoremenv[style=definitionframe]{roughDef}[equation]{Rough Definition}
  \crefname{roughDef}{Definition}{Definitions}
  \newtheorem{construction}[equation]{Construction}
  \newtheorem{notation}[equation]{Notation}
  \newtheorem{axiom}{Axiom}
  \newtheorem*{axiom*}{Axiom}

  \theoremstyle{remark}
  \newtheorem{remark}[equation]{Remark}
  \newtheorem{warning}[equation]{Warning}
%  \newtheorem{exercise}[equation]{Exercise}


% Solution environment

\newcommand{\finishSolutionChapter}{
%\vfill\hrulefill\\\noindent
%\arabic{solcounterlocal} exercises in Chapter \arabic{section}, and \arabic{solcounterglobal} total exercises up to this point.
%
\clearpage
}

\makeatletter
\newcommand{\nolisttopbreak}{\nobreak\@afterheading}
\makeatother

\newcounter{solcounterlocal}[section]
\newcounter{solcounterglobal}

\newcommand{\sol}[4][noprint]{

\stepcounter{solcounterlocal}\stepcounter{solcounterglobal}

\noindent\ignorespacesafterend\emph{Solution to} \cref{#2}.%
\nopagebreak%
\ifthenelse{\equal{#1}{print}}{
\nopagebreak%
\begin{mdframed}[backgroundcolor=examplecolor,linewidth=0pt]%
#3%
\end{mdframed}%
\nopagebreak
}{}%
\nolisttopbreak
\begin{description}[leftmargin=2.5ex,itemindent=0pt,topsep=0ex,nosep]
\item\nopagebreak
#4
\end{description}
\bigskip
}

% Aligned tikz environment
\newenvironment{altikz}{
\begin{aligned}
\begin{tikzpicture}
}
{
\end{tikzpicture}
\end{aligned}
}

%Getting ending symbols in example and exercise environments:

\newmdtheoremenv[style=exampleframe]{example}[equation]{Example}

\newtheorem{exc-inner}[equation]{Exercise}
\newenvironment{exercise}[1][]{
  \def\qedsymbol{$\lozenge$}% Set the QED symbol.
  \pushQED{\qed}
  \begin{exc-inner}[#1]~
}{
  \popQED
  \end{exc-inner}
}
  \crefname{exercise}{Exercise}{Exercises}


% Adjunctions
\newcommand{\adj}[5][30pt]{%[size] Cat L, Left, Right, Cat R.
\begin{tikzcd}[ampersand replacement=\&, column sep=#1]
  #2\ar[r, bend left=15, shift left=2pt, "#3"]
  \ar[r, Rightarrow, shorten <=8pt, shorten >=8pt]\&
  #5\ar[l, bend left=15, shift left=2pt, "#4"]
\end{tikzcd}
}

\newcommand{\adjr}[5][30pt]{%[size] Cat R, Right, Left, Cat L.
\begin{tikzcd}[ampersand replacement=\&, column sep=#1]
  #2\ar[r, bend left=15, shift left=2pt, "#3"]\&
  #5\ar[l, bend left=15, shift left=2pt, "#4"]
  \ar[l, Rightarrow, shorten <=8pt, shorten >=8pt]
\end{tikzcd}
}


%-------- Single symbols --------%

\DeclareSymbolFont{stmry}{U}{stmry}{m}{n}
\DeclareMathSymbol\fatsemi\mathop{stmry}{"23}

\DeclareFontFamily{U}{mathx}{\hyphenchar\font45}
\DeclareFontShape{U}{mathx}{m}{n}{
      <5> <6> <7> <8> <9> <10>
      <10.95> <12> <14.4> <17.28> <20.74> <24.88>
      mathx10
      }{}
\DeclareSymbolFont{mathx}{U}{mathx}{m}{n}
\DeclareFontSubstitution{U}{mathx}{m}{n}
\DeclareMathAccent{\widecheck}{0}{mathx}{"71}


%-------- Renewed commands --------%

\renewcommand{\ss}{\subseteq}

%-------- Other Macros --------%

\DeclarePairedDelimiter{\pair}{\langle}{\rangle}
\DeclarePairedDelimiter{\copair}{[}{]}
\DeclarePairedDelimiter{\floor}{\lfloor}{\rfloor}
\DeclarePairedDelimiter{\ceil}{\lceil}{\rceil}

\DeclarePairedDelimiter{\corners}{\ulcorner}{\urcorner}




\DeclareMathOperator{\Hom}{Hom}
\DeclareMathOperator{\Mor}{Mor}
\DeclareMathOperator{\dom}{dom}
\DeclareMathOperator{\cod}{cod}
\DeclareMathOperator*{\colim}{colim}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\Ob}{Ob}
\DeclareMathOperator{\Tr}{Tr}
\DeclareMathOperator{\dju}{\sqcup}

\newcommand{\const}[1]{\mathtt{#1}}%a constant, or named element of a set
\newcommand{\Set}[1]{\mathrm{#1}}%a named set
\newcommand{\cat}[1]{\mathcal{#1}}%a generic category
\newcommand{\Cat}[1]{\mathbf{#1}}%a named category
\newcommand{\fun}[1]{\textit{#1}}%function
\newcommand{\Fun}[1]{\mathsf{#1}}%functor

\newcommand{\id}{\mathrm{id}}
\newcommand{\cocolon}{:\!}
\newcommand{\iso}{\cong}
\newcommand{\too}{\longrightarrow}
\newcommand{\tto}{\rightrightarrows}
\newcommand{\To}[1]{\xrightarrow{#1}}
\newcommand{\Tto}[3][13pt]{\begin{tikzcd}[sep=#1, cramped, ampersand replacement=\&, text height=1ex, text depth=.3ex]\ar[r, shift left=2pt, "#2"]\ar[r, shift right=2pt, "#3"']\&{}\end{tikzcd}}
\newcommand{\Too}[1]{\xrightarrow{\;\;#1\;\;}}
\newcommand{\from}{\leftarrow}
\newcommand{\From}[1]{\xleftarrow{#1}}
\newcommand{\Fromm}[1]{\xleftarrow{\;\;#1\;\;}}
\newcommand{\surj}{\twoheadrightarrow}
\newcommand{\inj}{\rightarrowtail}
\newcommand{\wavyto}{\rightsquigarrow}
\newcommand{\lollipop}{\multimap}
\newcommand{\pr}{\mathrm{pr}}
\newcommand{\tickar}{\begin{tikzcd}[baseline=-0.5ex,cramped,sep=small,ampersand
replacement=\&]{}\ar[r,tick]\&{}\end{tikzcd}}
\newcommand{\imp}{\Rightarrow}
\renewcommand{\iff}{\Leftrightarrow}
\renewcommand{\th}{\ensuremath{^\tn{th}}\ }
\newcommand{\down}{\mathbin{\downarrow}}
\newcommand{\then}{\mathbin{\scalebox{.8}{/\!\!/}}}
\newcommand{\op}{^\tn{op}}
\newcommand{\grph}[1]{{#1}_{\mathrm{Gr}}}

\newcommand{\tn}[1]{\textnormal{#1}}
\newcommand{\ol}[1]{\overline{#1}}
\newcommand{\ul}[1]{\underline{#1}}
\newcommand{\wt}[1]{\widetilde{#1}}
\newcommand{\wh}[1]{\widehat{#1}}
\newcommand{\ubar}[1]{\underaccent{\bar}{#1}}
\newcommand{\LMO}[2][over]{\ifthenelse{\equal{#1}{over}}{\overset{#2}{\bullet}}{\underset{#2}{\bullet}}}
\newcommand{\LTO}[2][\bullet]{\overset{\tn{#2}}{#1}}


\newcommand{\NN}{\mathbb{N}}
\newcommand{\bb}{\mathbb{B}}
\newcommand{\BB}{\mathbb{B}}
\newcommand{\nn}{\NN}
%\newcommand{\PP}{\mathbb{P}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\zz}{\mathbb{Z}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\rr}{\mathbb{R}}
\newcommand{\IR}{\mathbb{I}\hspace{.6pt}\mathbb{R}}
\newcommand{\oo}{\mathcal{O}}
\newcommand{\singleton}{\{1\}}
\newcommand{\powset}{\Fun{P}}
\newcommand{\upset}{\Fun{U}}
\newcommand{\beh}{\Fun{B}}
\newcommand{\prt}[1]{\Fun{Prt}(#1)}

\newcommand{\upclose}{\mathop{\uparrow}}

\newcommand{\foo}{\const{foo}}

\newcommand{\inv}{^{-1}}

\newcommand{\inp}[1]{#1^\tn{in}}
\newcommand{\outp}[1]{#1^\tn{out}}

\newcommand{\TFS}{\Cat{TFS}}
\newcommand{\Grph}{\Cat{Grph}}
\newcommand{\SMC}{\Cat{SMC}}
\newcommand{\smset}{\Cat{Set}}
\newcommand{\smcat}{\Cat{Cat}}
\newcommand{\Bx}{\Set{Box}}
\newcommand{\Op}{\Cat{Op}}
\newcommand{\Shv}{\Cat{Shv}}
\newcommand{\true}{\const{true}}
\newcommand{\false}{\const{false}}
\newcommand{\Bool}{\Cat{Bool}}
%\newcommand{\Prob}{\Cat{Prob}}
\newcommand{\Cost}{\Cat{Cost}}
\newcommand{\List}{\Fun{List}}
\newcommand{\inst}{\tn{-}\Cat{Inst}}
\newcommand{\mat}{\Cat{Mat}}
\newcommand{\corel}[1]{\Cat{Corel}_{#1}}
\newcommand{\rel}{\Cat{Rel}}
\newcommand{\cospan}[1]{\Cat{Cospan}_{#1}}
\newcommand{\finset}{\Cat{FinSet}}

% Collaborative design{
  \newcommand{\Prof}{\Cat{Prof}}
  \newcommand{\Feas}{\Cat{Feas}}
  \newcommand{\Unit}[1]{\mathrm{U}_{#1}}
  \newcommand{\comp}[1]{\widehat{#1}}
  \newcommand{\conj}[1]{\widecheck{#1}}
  \newcommand{\col}[1]{\mathrm{col(#1)}}
%}

\newcommand{\cp}{\mathbin{\fatsemi}}


% Signal flow graphs{
\newcommand{\pgin}{\fun{in}}
\newcommand{\pgout}{\fun{out}}
\newcommand{\ord}[1]{\underline{{#1}}}
\newcommand{\free}{\Cat{Free}}
\newcommand{\expr}{\mathrm{Expr}}
\newcommand{\sfg}{\mathbf{SFG}}
\newcommand\addgen{\lower8pt\hbox{$\includegraphics[height=0.7cm]{pics/add.pdf}$}}
\newcommand\zerogen{\lower5pt\hbox{$\includegraphics[height=0.5cm]{pics/zero.pdf}$}}
%\newcommand\copygen{\lower8pt\hbox{$\includegraphics[height=0.7cm]{pics/copy.pdf}$}}
%\newcommand\discardgen{\lower5pt\hbox{$\includegraphics[height=0.5cm]{pics/discard.pdf}$}}
\newcommand\delaygen{\lower6pt\hbox{$\includegraphics[height=0.6cm]{pics/delay.pdf}$}}
%\newcommand\minonegen{\lower6pt\hbox{$\includegraphics[height=0.6cm]{pics/minone.pdf}$}}
%\newcommand\delayopgen{\lower6pt\hbox{$\includegraphics[height=0.6cm]{pics/delayop.pdf}$}}
\newcommand\scalargen{\lower6pt\hbox{$\includegraphics[height=0.6cm]{pics/scalar.pdf}$}}
%\newcommand\addopgen{\lower8pt\hbox{$\includegraphics[height=0.7cm]{pics/addop.pdf}$}}
%\newcommand\zeroopgen{\lower5pt\hbox{$\includegraphics[height=0.5cm]{pics/zeroop.pdf}$}}
\newcommand\copyopgen{\lower8pt\hbox{$\includegraphics[height=0.7cm]{pics/copyop.pdf}$}}
\newcommand\discardopgen{\lower5pt\hbox{$\includegraphics[height=0.5cm]{pics/discardop.pdf}$}}
%\newcommand\scalaropgen{\lower6pt\hbox{$\includegraphics[height=0.6cm]{pics/scalarop.pdf}$}}
%\newcommand\delaygenl{\lower6pt\hbox{$\includegraphics[height=0.6cm]{pics/delayl.pdf}$}}
%\newcommand\delayopgenl{\lower6pt\hbox{$\includegraphics[height=0.6cm]{pics/delayopl.pdf}$}}
%\newcommand\delaygenk{\lower6pt\hbox{$\includegraphics[height=0.6cm]{pics/delayk.pdf}$}}
%\newcommand\delayopgenk{\lower6pt\hbox{$\includegraphics[height=0.6cm]{pics/delayopk.pdf}$}}
\newcommand\twist{\lower6pt\hbox{$\includegraphics[height=0.6cm]{pics/twist.pdf}$}}
%\newcommand\id{\lower3pt\hbox{$\includegraphics[height=0.3cm]{pics/id.pdf}$}}

\tikzstyle{none}=[inner sep=0pt]
\tikzstyle{circ}=[circle,fill=black,draw,inner sep=3pt]
\tikzstyle{circw}=[circle,fill=white,draw,inner sep=3pt,thick]

%%fakesubsubsection generators
%}

% Circuits
\newcommand{\oprdset}{\mathbf{Set}}
\newcommand{\oprdcospan}{\mathbf{Cospan}}
\newcommand{\light}{\texttt{light}}
\newcommand{\switch}{\texttt{switch}}
\newcommand{\battery}{\texttt{battery}}
\newcommand{\elec}{\Fun{Circ}}

% Topos temporal logic{
\newcommand{\restrict}[2]{#1\big|_{#2}}
\newcommand{\Prop}{\const{Prop}}
\newcommand{\Time}{\const{Time}}
%}

\newcommand{\boxCD}[2][black]{\fcolorbox{#1}{white}{\begin{varwidth}{\textwidth}\centering #2\end{varwidth}}}

\newcommand{\?}{{\color{gray}{?}}}
\newcommand{\DNE}{{\color{gray}{\boxtimes}}}

\newcommand{\erase}[2][]{{\color{red}#1}}
\newcommand{\showhide}[2]{#1}
\newcommand{\overtime}[1]{{\color{gray}#1}}

\linespread{1.15}
%\allowdisplaybreaks
\setsecnumdepth{subsubsection}
\settocdepth{subsection}
\setlength{\parindent}{15pt}
\setcounter{tocdepth}{1}

%\newcommand{\define}[1]{\textbf{#1}}


\DeclareMathVersion{normal2}

%--------------- Document ---------------%
\begin{document}

\frontmatter

\title{\titlefont Seven Sketches in Compositionality:\\\LARGE Real-world models of category theory}
\title{\titlefont Seven Sketches in Compositionality:\\\LARGE Category theory in the real world}
\title{\titlefont Seven Sketches in Compositionality:\\\LARGE Category theoretic foundations of real-world phenomena}
\title{\titlefont Seven Sketches in Compositionality:\\\LARGE Toward a category-theoretic foundation for science and engineering}
\title{\titlefont Seven Sketches in Compositionality:\\\LARGE Real-World Applications of Category Theory}
\title{\titlefont Seven Sketches in Compositionality:\\\medskip\huge An Invitation to Categorical Modeling}
\title{\titlefont Seven Sketches in Compositionality:\\\medskip\huge An Invitation to Applied Category Theory}

\author{\LARGE Brendan Fong \and \LARGE David I. Spivak}

\posttitle{
  \vspace{.8in}
  \[
  \begin{tikzpicture}[oriented WD, bb min width =1cm, bbx=1cm, bb port sep =1, bb port length=2pt, bby=1ex]
    \node[coordinate] at (0,0) (ul) {};
    \node[coordinate] at (8,-12) (lr) {};
    \node[bb={0}{0}, rounded corners=5pt, drop shadow, top color=blue!5, fit = (ul) (lr)] (Z) {};
    \node[bb={2}{2}, green!25!black, drop shadow, fill=green!10, below right=2 and 0 of ul] (X11) {};
    \node[bb={3}{3}, green!25!black, drop shadow, fill=green!5, below right=of X11] (X12) {};
    \node[bb={2}{1}, green!25!black, drop shadow, fill=yellow!15, above right=of X12] (X13) {};
    \node[bb={2}{2}, green!25!black, drop shadow, fill=orange!15, below right = -1 and 1.5 of X12] (X21) {};
    \node[bb={1}{2}, red!75!black, drop shadow, fill=red!10, above right=-1 and 1 of X21] (X22) {?};
    \draw (X21_out1) to (X22_in1);
    \draw[ar] let \p1=(X22.north east), \p2=(X21.north west), \n1={\y1+\bby}, \n2=\bbportlen in
      (X22_out1) to[in=0] (\x1+\n2,\n1) -- (\x2-\n2,\n1) to[out=180] (X21_in1);
    \draw (X11_out1) to (X13_in1);
    \draw (X11_out2) to (X12_in1);
    \draw (X12_out1) to (X13_in2);
    \draw (Z.west|-X11_in2) to (X11_in2);
    \draw (Z.west|-X12_in2) to (X12_in2);
    \draw (X12_out2) to (X21_in2);
    \draw (X21_out2) to (Z.east|-X21_out2);
    \draw[ar] let \p1=(X12.south east), \p2=(X12.south west), \n1={\y1-\bby}, \n2=\bbportlen in
      (X12_out3) to[in=0] (\x1+\n2,\n1) -- (\x2-\n2,\n1) to[out=180] (X12_in3);
    \draw[ar] let \p1=(X22.north east), \p2=(X11.north west), \n1={\y2+\bby}, \n2=\bbportlen in
      (X22_out2) to[in=0] (\x1+\n2,\n1) -- (\x2-\n2,\n1) to[out=180] (X11_in1);
    \draw[ar] (X13_out1) to (Z.east|-X13_out1);
  \end{tikzpicture}
  \]
  \vspace{.5in}
  \endgroup
}


\date{\vfill (Last updated: \today)}

\maketitle
\thispagestyle{empty}

\subfile{C0-Preface}

\clearpage
\tableofcontents*

\mainmatter
\subfile{C1-Cascade_effects}

\subfile{C2-Resource_theory}

\subfile{C3-Data_transformations}

\subfile{C4-Collaborative_design}

\subfile{C5-Signal_flow_graphs}

\subfile{C6-Electric_circuits}

\subfile{C7-Logic_of_behavior}

\appendix
\begingroup
\footnotesize

\subfile{C8a-SolutionsA}

\subfile{C8b-SolutionsB}
\endgroup

\backmatter
%\appendix
%\subfile{C8-Sample_outlines}

\printbibliography
\printindex

\end{document}
